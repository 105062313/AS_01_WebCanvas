# Software Studio 2018 Spring Assignment 01 Web Canvas

## Web Canvas
<img src="example01.gif" width="700px" height="500px"></img>

## Todo
1. **Fork the repo ,remove fork relationship and change project visibility to public.**
2. Create your own web page with HTML5 canvas element where we can draw somethings.
3. Beautify appearance (CSS).
4. Design user interaction widgets and control tools for custom setting or editing (JavaScript).
5. **Commit to "your" project repository and deploy to Gitlab page.**
6. **Describing the functions of your canvas in REABME.md**

## Scoring (Check detailed requirments via iLMS)

|                       **Item**                   | **Score** |
|:--------------------------------------------:|:-----:|
|               Basic components               |  60%  |
|                 Advance tools                |  35%  |
|            Appearance (subjective)           |   5%  |
| Other useful widgets (**describe on README.md**) | 1~10% |

## Reminder
* Do not make any change to our root project repository.
* Deploy your web page to Gitlab page, and ensure it works correctly.
    * **Your main page should be named as ```index.html```**
    * **URL should be : https://[studentID].gitlab.io/AS_01_WebCanvas**
* You should also upload all source code to iLMS.
    * .html or .htm, .css, .js, etc.
    * source files
* **Deadline: 2018/04/05 23:59 (commit time)**
    * Delay will get 0 point (no reason)
    * Copy will get 0 point
    * "屍體" and 404 is not allowed


## Report
### 功能介紹
* 功能：eraser,brush,text,circle,triangle,square,download,undo,redo
    * eraser:可調整大小
    * brush:可調整顏色、粗細
    * text:可輸入內容、調整字體大小與字體，在畫布上按下滑鼠字便會出現
    * circle:可調整顏色，在畫布上按下滑鼠circle便會出現
    * triangle:可調整顏色，在畫布上按下滑鼠triangle便會出現
    * square:可調整顏色，在畫布上按下滑鼠square便會出現
    * download:按下左下角“SAVE IMAGE”即可下載成“dl.jpg“(適用於chrome)
    * undo:按下undo回到上一步
    * redo:按下redo取消undo
* eraser,brush,text,circle,triangle,square皆有專屬的cursor
* tools & 筆畫粗細 選取時按鈕會變紅色方便辨識

### function介紹

```javascript
function getMousePos(canvas, evt) {
  var rect = canvas.getBoundingClientRect();//getBoundingClientRect 取得物件完整座標資訊，包含寬高等
  return {
    x: evt.clientX - rect.left,
    y: evt.clientY - rect.top
  };
  //傳回滑鼠在canvas上的座標
}
```

```javascript
function mouseMove(evt) {
  var mousePos = getMousePos(canvas, evt);//透過getMousePos function 去取得滑鼠座標(x,y)
  ctx.lineTo(mousePos.x, mousePos.y);//利用取回的值畫線
  ctx.stroke();//畫
}
```

```javascript
canvas.addEventListener('mousedown', function(evt) {
  var mousePos = getMousePos(canvas, evt);//從按下去就會執行第一次的座標取得
  start_x=mousePos.x;
  start_y=mousePos.y;
  if(toolnum==1){//text功能
    ctx.fillStyle = "black";//字體顏色：黑
    ctx.font =  fontsize.value+"px "+font.value;//讀取並設置字體大小與字型
    ctx.fillText(text.value,mousePos.x, mousePos.y);//寫字
  }
  else if(toolnum==5){//畫circle
    ctx.beginPath();
    ctx.arc(mousePos.x,mousePos.y,radius,0,Math.PI*2);
    ctx.stroke();
  }
  else if(toolnum==4){//畫square
    ctx.beginPath();
    ctx.rect(mousePos.x,mousePos.y,radius,radius);
    ctx.stroke();
  }
  else if(toolnum==3){//畫triangle
    var path=new Path2D();
    path.moveTo(mousePos.x+(radius/2)+50,mousePos.y+radius/2);
    path.lineTo(mousePos.x+(radius/2),mousePos.y+(radius/2)-50);
    path.lineTo(mousePos.x+(radius/2)-50,mousePos.y+(radius/2));
    path.lineTo(mousePos.x+(radius/2)+50,mousePos.y+(radius/2));
    path.lineTo(mousePos.x+(radius/2),mousePos.y+(radius/2)-50);
    ctx.stroke(path);
  }

  else{//brush&eraser
    ctx.beginPath();
    ctx.moveTo(mousePos.x, mousePos.y);
    evt.preventDefault();
    canvas.addEventListener('mousemove', mouseMove, false);
    //mousemove的偵聽也在按下去的同時開啟
  }
});
```

```javascript
canvas.addEventListener('mouseup', function() {
  canvas.removeEventListener('mousemove', mouseMove, false);
  push();
}, false);
//如果滑鼠放開，將會停止mouseup的偵聽
```

```javascript
document.getElementById('reset').addEventListener('click', function() {
  //reset清空畫面
  ctx.clearRect(0, 0, canvas.width, canvas.height);
  ctx.fillStyle ="white";
  ctx.fillRect(0,0,800,500);
}, false);
```



```javascript
function push(){
    steps++;
    if(steps < array.length){
        array.length = steps;
    }
    array.push(canvas.toDataURL());
}

function undo(){//undo
    if(steps > 0){
        steps--;
        var canvasPic = new Image();
        canvasPic.src = array[steps];
        canvasPic.onload = function(){
            ctx.drawImage(canvasPic, 0, 0, canvas.width, canvas.height);
        }
    }
}

function redo(){//redo(取消redo)
    if(steps < array.length){
        steps++;
        var canvasPic = new Image();
        canvasPic.src = array[steps];
        canvasPic.onload = function(){
            ctx.drawImage(canvasPic, 0, 0, canvas.width, canvas.height);
        }
    }
}
```

