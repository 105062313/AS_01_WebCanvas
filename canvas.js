var canvas = document.getElementById('art');
var ctx = canvas.getContext('2d');

var start_x;
var start_y;
var radius=50;
var toolnum=0;
var array=new Array();
var steps=-1;
ctx.fillStyle ="white";
ctx.fillRect(0,0,800,500);
var undoButton = document.getElementById('undo');
var redoButton = document.getElementById('redo');

undoButton.addEventListener('click', undo);
redoButton.addEventListener('click', redo);

function push(){
    steps++;
    if(steps < array.length){
        array.length = steps;
    }
    array.push(canvas.toDataURL());
}

function undo(){
    if(steps > 0){
        steps--;
        var canvasPic = new Image();
        canvasPic.src = array[steps];
        canvasPic.onload = function(){
            ctx.drawImage(canvasPic, 0, 0, canvas.width, canvas.height);
        }
    }
}

function redo(){
    if(steps < array.length){
        steps++;
        var canvasPic = new Image();
        canvasPic.src = array[steps];
        canvasPic.onload = function(){
            ctx.drawImage(canvasPic, 0, 0, canvas.width, canvas.height);
        }
    }
}

document.getElementById("brush").addEventListener("click", function(){
  toolnum=0;
  canvas.style.cursor="url('img/brush1.png'),auto";
  brushx.style.backgroundColor = "red";
  wordx.style.backgroundColor = "white";
  trianglex.style.backgroundColor = "white";
  circlex.style.backgroundColor = "white";
  squarex.style.backgroundColor = "white";
  eraserx.style.backgroundColor = "white";
  ctx.strokeStyle = 'black';
  
});
document.getElementById("word").addEventListener("click", function(){
  toolnum=1;
  canvas.style.cursor="url('img/text1.png'),auto";
  brushx.style.backgroundColor = "white";
  wordx.style.backgroundColor = "red";
  trianglex.style.backgroundColor = "white";
  circlex.style.backgroundColor = "white";
  squarex.style.backgroundColor = "white";
  eraserx.style.backgroundColor = "white";
});
document.getElementById("eraser").addEventListener("click", function(){
  toolnum=2;
  canvas.style.cursor="url('img/eraser1.png'),auto";
  brushx.style.backgroundColor = "white";
  wordx.style.backgroundColor = "white";
  trianglex.style.backgroundColor = "white";
  circlex.style.backgroundColor = "white";
  squarex.style.backgroundColor = "white";
  eraserx.style.backgroundColor = "red";
  ctx.strokeStyle = 'white';
  
});
document.getElementById("triangle").addEventListener("click", function(){
  toolnum=3;
  canvas.style.cursor="url('img/triangle1.png'),auto";
  brushx.style.backgroundColor = "white";
  wordx.style.backgroundColor = "white";
  trianglex.style.backgroundColor = "red";
  circlex.style.backgroundColor = "white";
  squarex.style.backgroundColor = "white";
  eraserx.style.backgroundColor = "white";
  ctx.strokeStyle = 'black';
});
document.getElementById("square").addEventListener("click", function(){
  toolnum=4;
  canvas.style.cursor="url('img/square1.png'),auto";
  brushx.style.backgroundColor = "white";
  wordx.style.backgroundColor = "white";
  trianglex.style.backgroundColor = "white";
  circlex.style.backgroundColor = "white";
  squarex.style.backgroundColor = "red";
  eraserx.style.backgroundColor = "white";
  ctx.strokeStyle = 'black';
});
document.getElementById("circle").addEventListener("click", function(){
  toolnum=5;
  canvas.style.cursor="url('img/circle1.png'),auto";
  brushx.style.backgroundColor = "white";
  wordx.style.backgroundColor = "white";
  trianglex.style.backgroundColor = "white";
  circlex.style.backgroundColor = "red";
  squarex.style.backgroundColor = "white";
  eraserx.style.backgroundColor = "white";
  ctx.strokeStyle = 'black';
});


function getMousePos(canvas, evt) {
  var rect = canvas.getBoundingClientRect();
  return {
    x: evt.clientX - rect.left,
    y: evt.clientY - rect.top
  };
}

function mouseMove(evt) {
  var mousePos = getMousePos(canvas, evt);
  ctx.lineTo(mousePos.x, mousePos.y);
  ctx.stroke();
}

canvas.addEventListener('mousedown', function(evt) {
  var mousePos = getMousePos(canvas, evt);
  start_x=mousePos.x;
  start_y=mousePos.y;
  if(toolnum==1){
    ctx.fillStyle = "black";
    ctx.font =  fontsize.value+"px "+font.value;
    ctx.fillText(text.value,mousePos.x, mousePos.y);
  }
  else if(toolnum==5){
    ctx.beginPath();
    ctx.arc(mousePos.x,mousePos.y,radius,0,Math.PI*2);
    ctx.stroke();
  }
  else if(toolnum==4){
    ctx.beginPath();
    ctx.rect(mousePos.x,mousePos.y,radius,radius);
    ctx.stroke();
  }
  else if(toolnum==3){
    var path=new Path2D();
    path.moveTo(mousePos.x+(radius/2)+50,mousePos.y+radius/2);
    path.lineTo(mousePos.x+(radius/2),mousePos.y+(radius/2)-50);
    path.lineTo(mousePos.x+(radius/2)-50,mousePos.y+(radius/2));
    path.lineTo(mousePos.x+(radius/2)+50,mousePos.y+(radius/2));
    path.lineTo(mousePos.x+(radius/2),mousePos.y+(radius/2)-50);
    ctx.stroke(path);
  }

  else{
    ctx.beginPath();
    ctx.moveTo(mousePos.x, mousePos.y);
    evt.preventDefault();
    canvas.addEventListener('mousemove', mouseMove, false);
  }
});



canvas.addEventListener('mouseup', function() {
  canvas.removeEventListener('mousemove', mouseMove, false);
  push();
}, false);

document.getElementById('reset').addEventListener('click', function() {
  
  ctx.clearRect(0, 0, canvas.width, canvas.height);
  ctx.fillStyle ="white";
  ctx.fillRect(0,0,800,500);

}, false);




var text = document.getElementById('text');
var fontsize = document.getElementById('selector');
var font = document.getElementById('selector2');








var colors = ['red', 'blue', 'green', 'purple', 'yellow', 'orange', 'pink', 'black'];
var size = [1, 3, 5, 10, 15, 20];
var sizeNames = ['one', 'three', 'five', 'ten', 'fifteen', 'twenty'];


function listener(i) {
  document.getElementById(colors[i]).addEventListener('click', function() {
    ctx.strokeStyle = colors[i];
  }, false);
}

function fontSizes(i) {
  document.getElementById(sizeNames[i]).addEventListener('click', function() {
    ctx.lineWidth=size[i];
  }, false);
}






for(var i = 0; i < colors.length; i++) {
  listener(i);
}

for(var i = 0; i < size.length; i++) {
  fontSizes(i);
}

document.getElementById("one").addEventListener("click", function(){
  one.style.backgroundColor = "red";
  three.style.backgroundColor = "white";
  five.style.backgroundColor = "white";
  ten.style.backgroundColor = "white";
  fifteen.style.backgroundColor = "white";
  twenty.style.backgroundColor = "white";
});

document.getElementById("three").addEventListener("click", function(){
  one.style.backgroundColor = "white";
  three.style.backgroundColor = "red";
  five.style.backgroundColor = "white";
  ten.style.backgroundColor = "white";
  fifteen.style.backgroundColor = "white";
  twenty.style.backgroundColor = "white";
});
document.getElementById("five").addEventListener("click", function(){
  one.style.backgroundColor = "white";
  three.style.backgroundColor = "white";
  five.style.backgroundColor = "red";
  ten.style.backgroundColor = "white";
  fifteen.style.backgroundColor = "white";
  twenty.style.backgroundColor = "white";
});
document.getElementById("ten").addEventListener("click", function(){
  one.style.backgroundColor = "white";
  three.style.backgroundColor = "white";
  five.style.backgroundColor = "white";
  ten.style.backgroundColor = "red";
  fifteen.style.backgroundColor = "white";
  twenty.style.backgroundColor = "white";
});
document.getElementById("fifteen").addEventListener("click", function(){
  one.style.backgroundColor = "white";
  three.style.backgroundColor = "white";
  five.style.backgroundColor = "white";
  ten.style.backgroundColor = "white";
  fifteen.style.backgroundColor = "red";
  twenty.style.backgroundColor = "white";
});
document.getElementById("twenty").addEventListener("click", function(){
  one.style.backgroundColor = "white";
  three.style.backgroundColor = "white";
  five.style.backgroundColor = "white";
  ten.style.backgroundColor = "white";
  fifteen.style.backgroundColor = "white";
  twenty.style.backgroundColor = "red";
});



push();



